corepro-tagger
===

## Usage
 - Clone this repository
 - `$ chmod +x installer`
 - `$ ./installer`
 - Change `~/.ctconfig/settings.cfg` fields.
 - Then just run `corepro-tagger` anywhere 
  - If you want to merge branches into master, add them as parameter like `$ corepro-tagger branch1 branch2 ...`

